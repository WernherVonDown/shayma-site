## Local run

1. install docker-compose 
2. run `docker-compose -f docker-compose.local.yml up`
3. the site will be available on http://localhost:7000

## Deploy
1. `git add .` (add all changes)
2. `git commit -m "some message"` (commit changes)
3. `git push origin main` (push changes commited in main or it can be another branch)
